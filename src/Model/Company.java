package Model;

import Controller.Measurable;
import Controller.Taxable;

public class Company implements Taxable{
	private String nameCom;
	private double income;
	private double expense;
	
	public Company(String nameCom,double income, double expense){
		this.nameCom = nameCom;
		this.income = income;
		this.expense = expense;
	}
	
	//Override
	public double getTax(){
		double tax = (income - expense)*0.3;
	return tax;
	}
}