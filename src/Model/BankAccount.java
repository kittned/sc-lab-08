package Model;
import Controller.Measurable;
import Controller.Taxable;

public class BankAccount implements Measurable{
	private String nameCom;
	
	private double balance;
	
	public BankAccount(String nameCom, double balance){
		this.nameCom = nameCom;
		this.balance = balance;
	}
	
	//Override
	public double getMeasure(){
		return balance;
	}
	
	
}
