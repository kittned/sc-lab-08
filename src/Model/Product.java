package Model;

import Controller.Measurable;
import Controller.Taxable;

public class Product implements Taxable{
	private String nameGoods;
	private double price;
	
	public Product(String nameGoods,double price){
		this.nameGoods = nameGoods;
		this.price = price;
	}
	
	//Override
	public double getTax(){
		double tax = price*0.07;
		return tax;
	}
}
