package Model;
import Controller.Measurable;

public class Country implements Measurable{
	
	private String name;
	private double area;
	public Country(String aName, double anArea) { 
		this.name = aName;
		this.area = anArea; 
	}
	
	public String getName() {
		return name;
	}
	
	public double getArea(){
		return area;
	}
	
	public double getMeasure() {
		return area;
	}
}
