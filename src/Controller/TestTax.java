package Controller;

import java.util.ArrayList;

import Model.Company;
import Model.Person;
import Model.Product;

public class TestTax {	
	public static void main(String[] args){
		TaxCalculator cals = new TaxCalculator();
		TestTax test = new TestTax();
		test.testTaxs();
	}
	
	public void testTaxs(){
		ArrayList<Taxable> taxList = new ArrayList<>();
		taxList.add(new Person("John", 18000));
		taxList.add(new Person("Mary", 16500));
		taxList.add(new Company("Nanmee Book", 580000, 200000));
		taxList.add(new Company("Toyota", 4500000, 2500000));
		taxList.add(new Product("Chocolate", 39));
		taxList.add(new Product("Cake", 299));
		//System.out.println("  ### Test 3 ### ");
		//System.out.println(taxList);
		
	}
	
}
