package Controller;

public interface Taxable {
	double getTax();
}
