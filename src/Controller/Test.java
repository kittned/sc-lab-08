package Controller;
import Model.*;

public class Test {
	public static void main(String[] args){
		Data datas = new Data();
		Test test = new Test();
		test.testPerson();
		test.testMin();
		
	}
	public void testPerson(){
		Data datas = new Data();
		Measurable[] persons = new Measurable[3];
		persons[0] = new Person("John", 180);
		persons[1] = new Person("Mary", 165);
		persons[2] = new Person("Rick", 190);
		System.out.println(datas.average(persons));
	}
	
	public void testMin(){
		Data datas = new Data();
		Measurable[] persons = new Measurable[2];
		persons[0] = new Person("John", 180);
		persons[1] = new Person("Mary", 165);
		
		Measurable[] countries = new Measurable[2];
		countries[0] = new Country("Thailand", 200000);
		countries[1] = new Country("China", 5200000);
		
		Measurable[] bankAcc = new Measurable[2];
		bankAcc[0] = new BankAccount("Thaned", 2500);
		bankAcc[1] = new BankAccount("Sumintha", 1500);
		System.out.println("  ### Test 2 ### ");
		System.out.println(datas.min(persons[0], persons[1]));
		System.out.println(datas.min(countries[0], countries[1]));
		System.out.println(datas.min(bankAcc[0], bankAcc[1]));
	}
	
	
	
}