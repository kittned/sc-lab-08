package Controller;

public interface Measurable {
	double getMeasure();
}
